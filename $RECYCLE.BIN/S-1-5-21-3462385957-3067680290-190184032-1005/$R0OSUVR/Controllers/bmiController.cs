﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class bmiController : Controller
    {
        // GET: bmi
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(float h,float w)
        {
            float m_h = h / 100;
            float bmi = w / (m_h * m_h);

            string level = "";
            if (bmi < 18.5)
            {
                level = "太瘦";
            }
            else if (bmi > 18.5 && bmi < 35)
            {
                level = "適中";
            }
            else if (bmi > 35)
            {
                level = "太胖";
                
            }

                ViewBag.BMI = bmi;
                ViewBag.level = level;
            return View();
           
        }
    }
}
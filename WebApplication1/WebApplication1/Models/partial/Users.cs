﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [MetadataType(typeof(UsersMetadata))]


    public partial class Users
    {
    }
    public  class UsersMetadata
    {
       

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("姓名")]
        [StringLength(10)]
        public string name { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("信箱")]
        [StringLength(50)]
        public string Email { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("密碼")]
        [StringLength(10)]
        public string Password { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("生日")]
      
        public System.DateTime Barthday { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("性別")]
     
        public bool Gemder { get; set; }

    }
 
}